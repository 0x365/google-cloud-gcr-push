#!/usr/bin/env sh

# Build & Push an image to GCR, https://cloud.google.com/container-registry/
#
# Required globals:
#   KEY_FILE
#   PROJECT
#   NAME
#
# Option globals
#   IMAGE_PATH
#   TAG
#   LOCATION: us, eu, asia Default: us

# Sample to run locally

source "$(dirname "$0")/common.sh"
enable_debug

# mandatory parameters

export

KEY_FILE=${KEY_FILE:?'KEY_FILE variable missing.'}
PROJECT=${PROJECT:?'PROJECT variable missing.'}
NAME=${NAME:?'NAME variable missing.'}

# optionals parameters
IMAGE_PATH=${IMAGE_PATH:-'/'}
TAG=${TAG:-''}
LOCATION=${LOCATION:-'us'}
ENVIRONMENT=${ENVIRONMENT:-''}

info "Validating inputs variables"

# check location
case ${LOCATION} in
  us|eu|asia)
    echo "Location Valid"
  ;;
  *)
    fail "Location invalid"; 
  ;;
esac

CACHE_IFS=$IFS
export IFS=$'\n'

if [ "$ENVIRONMENT" != "" ] 
  then
    BUILD_ARGS=""
    VARIABLES=`echo $ENVIRONMENT | tr "," "\n"`
    for variable in $VARIABLES
      do
        # Trim $variable
        TMP_variable=`echo $variable | sed -e 's/^[[:space:]]*//'`
        BUILD_ARGS="${BUILD_ARGS} --build-arg \"${variable}\""
      done
  else
    BUILD_ARGS=""
fi

export IFS=$CACHE_IFS

if [ ${DEBUG} == "true" ]
  then
    echo $BUILD_ARGS
fi

info "Setting up environment".

echo "${KEY_FILE}" | base64 -d >> /tmp/key-file.json

run gcloud auth activate-service-account --key-file /tmp/key-file.json --quiet ${gcloud_debug_args}
run gcloud config set project $PROJECT --quiet ${gcloud_debug_args}
run gcloud auth configure-docker --quiet

info "Starting build image"

run docker build ${BUILD_ARGS} -t ${LOCATION}.gcr.io/${PROJECT}${IMAGE_PATH}${NAME} .
if [ ! -z "$TAG" ]
then
  run docker tag ${LOCATION}.gcr.io/${PROJECT}${IMAGE_PATH}${NAME} ${LOCATION}.gcr.io/${PROJECT}${IMAGE_PATH}${NAME}:${TAG}
fi

info "Starting push to Google Cloud Container Registry"

run docker push --all-tags ${LOCATION}.gcr.io/${PROJECT}${IMAGE_PATH}${NAME}

if [ "${status}" -eq 0 ]; then
  success "Push successful."
else
  fail "Push failed."
fi
