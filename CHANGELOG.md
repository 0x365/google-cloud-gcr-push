# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.1.6
- minor: Update cloud SDK

## 0.1.5
- minor: Add `--all-tags` parameter to docker push
- minor: Update cloud SDK and change from dockerhub to gcr.io

## 0.1.4
- minor: Update cloud SDK and change from dockerhub to gcr.io
- minor: Trim spaces in variables
- minor: Add DEBUG validations

## 0.1.3
- minor: fix issue with space in arguments buils.
- minor: update gcloud sdk version.

## 0.1.2

- minor: Update sdk-cloud version to 295
- minor: Add support to environment variables as build arguments

## 0.1.1

- minor: Fix documentation, code clean up.

## 0.1.0

- minor: Initial release of Bitbucket Pipeline for Google Cloud Container Registry push.
