FROM gcr.io/google.com/cloudsdktool/cloud-sdk:432.0.0-alpine

COPY pipe /usr/bin/
RUN chmod +x /usr/bin/pipe.sh

ENTRYPOINT ["/usr/bin/pipe.sh"]
