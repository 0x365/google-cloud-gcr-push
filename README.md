# Bitbucket Pipelines Pipe: Push Docker Image to Google Cloud Container Registry

This is an pipe to build and push a docker image to [Google Cloud Container Registry][gcloud-container-registry].

This pipe is based on [google-cloud-storage-deploy](https://bitbucket.org/atlassian/google-cloud-storage-deploy/src/master/) pipe, this is not an atlassian official pipe, but at this moment not exist a pipeline to build and push an image to gcr.io

## Requirements

- Billing need to be enabled in your project.
- [Enable the container registry API](https://console.cloud.google.com/apis/library/containerregistry.googleapis.com?_ga=2.164507324.2026007562.1590700074-677121002.1579570359)

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: 0x365/google-cloud-gcr-push:0.1.6
  variables:
    KEY_FILE: '<string>'
    PROJECT: '<string>'
    NAME: '<string>'
    # IMAGE_PATH: '<string>'
    # TAG: '<string>'
    # LOCATION: '<string>'
    # ENVIRONMENT: '<string>'
```

## Variables

| Variable                   | Usage                                                |
| ----------------------------- | ---------------------------------------------------- |
| KEY_FILE (*)                  |  base64 encoded Key file for a [Google service account](https://cloud.google.com/iam/docs/creating-managing-service-account-keys). |
| PROJECT (*)                   |  The project that owns the google container registry to push into. |
| NAME (*)                    |  Docker image name. |
| IMAGE_PATH                    |  Internal folder in google container registry |
| TAG                    |  Create a tag to the image |
| LOCATION                           |  Is the location of the storage bucket. Valid values are: `us`, `eu`, `asia` |
| ENVIRONMENT                    |  Define environment variables. format: `VAR1=VALUE1,VAR2=VALUE2,VAR3=VALUE3` |

_(*) = required variable._

## Examples

### Basic example:

```yaml
script:
  - pipe: 0x365/google-cloud-gcr-push:0.1.6
    variables:
      KEY_FILE: $KEY_FILE
      PROJECT: 'my-project'
      NAME: 'my-docker-image'
```

### Advanced example: 
    
```yaml
script:
  - pipe: 0x365/google-cloud-gcr-push:0.1.6
    variables:
      KEY_FILE: $KEY_FILE
      PROJECT: 'my-project'
      NAME: 'my-docker-image'
      IMAGE_PATH: '/demo/'
      TAG: 'latest'
      LOCATION: 'asia'
      ENVIRONMENT: 'VAR1=VALUE1,
                    VAR2=VALUE2,
                    VAR3=VALUE3'
```
## Trobleshooting

- Container ID Cannot Be Mapped to Host ID Error: this is due an bug in bitbucket pipelines, more info in the following URLS:
  - https://jira.atlassian.com/browse/BCLOUD-17319
  - https://github.com/phusion/passenger-docker/issues/235

## Support
If you’d like help with this pipe, or you have an issue or feature request, send me an email to alexander.moreno at 0x365.com or 

If you’re reporting an issue, please include:

* the version of the pipe
* relevant logs and error messages
* steps to reproduce


## License
Copyright (c) 2020 Alexander Moreno Delgado and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.